<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('habit/index', 'HabitController@index');
Route::get('habit/create', 'HabitController@create');
Route::post('habit/store', 'HabitController@store');
Route::get('habit/show', 'HabitController@show');
Route::get('habit/edit', 'HabitController@edit');
Route::post('habit/update', 'HabitController@update');
Route::delete('habit/destroy', 'HabitController@destroy');

Route::get('track/index', 'TrackController@index');
Route::get('track/create', 'TrackController@create');
Route::post('track/store', 'TrackController@store');
Route::get('track/show', 'TrackController@show');
Route::get('track/edit', 'TrackController@edit');
Route::post('track/update', 'TrackController@update');
Route::delete('track/destroy', 'TrackController@destroy');
