<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Track extends Model
{
    protected $fillables = [
        'habit_id',
    	'location',
        'dateTime',
        'emotion',
        'socialContext',
        'immediatePrecedingAction',
    ];

    public function habit()
    {
        return $this->hasOne('App\Habit');
    }
}
