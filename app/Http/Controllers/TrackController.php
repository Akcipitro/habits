<?php

namespace App\Http\Controllers;

use App\Habit;
use App\Track;
use App\User;
use Illuminate\Http\Request;

class TrackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\Habit $habit
     * @return \Illuminate\Http\Response
     */
    public function index(Habit $habit)
    {
        $user = User::find(Auth::id());
        $tracks = Track::where('habit_id', $habit->id);

        return view('trackIndex', compact('user', 'tracks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Habit $habit
     * @return \Illuminate\Http\Response
     */
    public function create(Habit $habit)
    {
        return view('track/create', compact('habit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'habit_id' => 'required',
            'location' => 'required',
            'dateTime' => 'required',
            'emotion' => 'required',
            'socialContext' => 'required',
            'immediatePrecedingAction' => 'required'
        ]);

        $track = new Track;
        $track->habit_id = $request->habit_id;
        $track->location = $request->location;
        $track->dateTime = $request->dateTime;
        $track->socialContext = $request->socialContext;
        $track->immediatePrecedingAction = $request->immediatePrecedingAction;
        $track->save();

        return redirect()->action('TrackController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function show(Track $track)
    {
        return view('track/show', compact('track'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function edit(Track $track)
    {
        return view('track/edit', compact('track'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Track  $track
     * @return \Illuminate\Http\Response
     * @todo elegant handling of Track object
     */
    public function update(Request $request, Track $track)
    {
        $this->validate($request, [
            'track _id' => 'required',
            'habit_id' => 'required',
            'location' => 'required',
            'dateTime' => 'required',
            'emotion' => 'required',
            'socialContext' => 'required',
            'immediatePrecedingAction' => 'required'
        ]);
        $track = Track::where('id', $request->track_id);
        $track->habit_id = $request->habit_id;
        $track->location = $request->location;
        $track->dateTime = $request->dateTime;
        $track->emotion = $request->emotion;
        $track->socialContext = $request->socialContext;
        $track->immediatePrecedingAction = $request->immediatePrecedingAction;
        $track->save();

        return redirect()->action('TrackController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function destroy(Track $track)
    {
        $track->delete();

        return redirect()->action('TrackController@index');
    }
}
