<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Habit extends Model
{
    protected $fillable = [
        'track_id',
        'user_id',
        'name',
    ];

    public function track()
    {
        return $this->hasMany('App\Track');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
